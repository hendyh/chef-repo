current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "admin"
client_key               "#{current_dir}/admin.pem"
chef_server_url          "https://ec2-18-220-143-190.us-east-2.compute.amazonaws.com/organizations/hendylab"
cookbook_path            ["~/workspace/chef-repo/cookbooks"]
knife[:editor] = "C:\\app\\vim\\gvim.exe"

