default['web']['document_root'] = '/var/www/html'
default['web']['log_dir'] = '/var/log/apache2'
default['web']['home_page'] = '/var/www/html/index.html'
default['httpd']['service_name'] = 'default'