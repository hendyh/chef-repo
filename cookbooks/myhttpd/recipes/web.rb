#
# Cookbook:: myhttpd
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

# Create the document root directory.
directory node['web']['document_root'] do
  recursive true
end

file node['web']['home_page']  do
  content '<html>This is a placeholder for the home page.</html>'
  mode '0755'
end 

# Create the log directory.
#directory node['web']['log_dir'] do
#  recursive true
#end

# Add the site configuration.
httpd_config 'default' do
  source 'mysite.cnf.erb'
end

# Install Apache and start the service.
httpd_service 'default' do
  mpm 'prefork'
  listen_ports ['81','82']
  action [:create, :start]
  subscribes :restart, 'httpd_config[default]'
end
